package br.com.frwkapp.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.OncePerRequestFilter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

@Component
public class SecurityFilter extends OncePerRequestFilter {

	public static final String SECRET = "framework";
	private static final String HEADER_STRING = "Authorization";
	private static final String CABECALHO_DE_AUTORIZACAO_INVALIDO = "Authorization header must be provided";
	
	private List<String> publicPaths = Arrays.asList(
            "oauth",
            "/swagger-resources",
            "/swagger-ui.html",
            "/webjars",
            "swagger.index",
            "swagger.json",
            "/health",
            "/partners/me",
            "/me",
            "/auth",
            "/auth/refresh",
            "/auth/me",
            "/user"
    );

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		try {
			System.out.println("#login security filter");
			Optional<String> optionalToken = getToken((HttpServletRequest) request);
			optionalToken.ifPresent(token -> {
	
				token = token.replace("Bearer ", "");
			    token = CriptexFrw.decrypt(token);
				Claims claims = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody();

				@SuppressWarnings("unchecked")
				List<String> roles = claims.get("roles", List.class);
				List<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
				
				roles.forEach(role -> {
					authorities.add(new SimpleGrantedAuthority(role));
				});
	
				UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = 
						new UsernamePasswordAuthenticationToken(claims.getSubject(), null, authorities);
				usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
				System.out.println("#login autenticado");
			});
			System.out.println("#login não autenticado");
			filterChain.doFilter(request, response);
		} catch (Exception ex) {
			sendHttpForbidden(response);
		}
	}

	protected Optional<String> getToken(HttpServletRequest request) {
		return Optional.ofNullable(request.getHeader(HEADER_STRING));
	}

	protected void sendHttpForbidden(ServletResponse response) throws IOException {
		((HttpServletResponse) response).sendError(HttpServletResponse.SC_FORBIDDEN, CABECALHO_DE_AUTORIZACAO_INVALIDO);
	}
	
	private boolean anyMatch(List<String> paths, String fullPath) {
        return paths.stream().anyMatch(fullPath::contains);
    }
	
	@Bean
	public CorsWebFilter corsWebFilter() {
		CorsConfiguration corsConfiguration = new CorsConfiguration();
		corsConfiguration.setAllowCredentials(true);
		corsConfiguration.addAllowedHeader("*");
		corsConfiguration.addAllowedMethod("*");
		corsConfiguration.addAllowedOrigin("*");
		UrlBasedCorsConfigurationSource corsConfigurationSource = new UrlBasedCorsConfigurationSource();
		corsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);
		return new CorsWebFilter(corsConfigurationSource);
	}
}