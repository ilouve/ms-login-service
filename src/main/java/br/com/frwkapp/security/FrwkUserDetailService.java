package br.com.frwkapp.security;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.frwkapp.model.repository.UserRepository;
import io.jsonwebtoken.lang.Collections;

@Service
public class FrwkUserDetailService implements UserDetailsService {

	@Autowired
	private UserRepository repository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<br.com.frwkapp.model.domain.UserAuth> optionalUsuario = repository.findOne(Example.of(new br.com.frwkapp.model.domain.UserAuth(username)));
		return getUserDetails(optionalUsuario.get());
	}

	protected UserDetails getUserDetails(br.com.frwkapp.model.domain.UserAuth user) {
		List<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();

		if (!Collections.isEmpty(user.getRoles())) {
			user.getRoles().forEach(role -> {
				authorities.add(new SimpleGrantedAuthority(role));
			});			
		}

		return new User(user.getLogin(), user.getPassword(), authorities);
	}
}