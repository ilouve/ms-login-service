package br.com.frwkapp.model.dto;

import java.util.List;

import br.com.frwkapp.abstracts.BaseDTO;
import br.com.frwkapp.model.domain.UserAuth;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class UserDTO extends BaseDTO {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String login;
    private String password;
    private String name;
    private List<String> roles;

    public UserDTO(UserAuth entity) {
        super(entity);
        this.login = entity.getLogin();
        this.name = entity.getName();
        this.roles = entity.getRoles();
    }
 
    public UserDTO() {}
}