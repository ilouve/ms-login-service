package br.com.frwkapp.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.frwkapp.abstracts.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "t_tenant")
@AllArgsConstructor
@NoArgsConstructor
public class Tenant extends BaseEntity	 {

    /**
     * 
     */
    private static final long serialVersionUID = -8311929286368836531L;

    @Id
    @GeneratedValue(generator = "SQ_TENANT")
    @SequenceGenerator(name = "SQ_TENANT", sequenceName = "SQ_TENANT", allocationSize = 1)
    @Column(name = "id")
    private Long id;
    
    @Column(name = "name")
    private String name;
	
    public Tenant(Long id) {
        this.id = id;
    }
}