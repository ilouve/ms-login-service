package br.com.frwkapp.model.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.frwkapp.abstracts.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "t_user_auth")
public class UserAuth extends BaseEntity {


    @Id
    @GeneratedValue(generator = "sq_user_auth")
    @SequenceGenerator(name = "sq_user_auth", sequenceName = "sq_user_auth", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "name")
    private String name;

    @ElementCollection
    @CollectionTable(name = "t_user_auth_role", joinColumns = @JoinColumn(name="fk_user"))
    @Column(name="role")
    private List<String> roles;
    
    @ElementCollection
    @CollectionTable(name = "t_user_auth_device", joinColumns = @JoinColumn(name="fk_user"))
    @Column(name="device")
    private List<String> devices = new ArrayList<>();
    
    public UserAuth() {    	
    }

    public UserAuth(String username) {
    	this.login = username;
    }

}