package br.com.frwkapp.model.repository;

import org.springframework.stereotype.Repository;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.model.domain.UserAuth;

@Repository
public interface UserRepository extends BaseRepository<UserAuth> {

	public UserAuth findByLoginAndPassword(String login, String password);
	public UserAuth findByLogin(String login);
}