package br.com.frwkapp.model.exception;

public class UserNotFoundException extends RuntimeException {

	public UserNotFoundException() {
		super("Não foi possivel encontrar o usuário com as credenciais informadas.");
	}

}
