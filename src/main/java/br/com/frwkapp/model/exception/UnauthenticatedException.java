package br.com.frwkapp.model.exception;

import org.hibernate.service.spi.ServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class UnauthenticatedException extends ServiceException {

    public UnauthenticatedException() {
        super("Unauthenticated");
    }
}
