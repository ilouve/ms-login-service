package br.com.frwkapp.model.exception;

import org.hibernate.service.spi.ServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BadRequestException extends ServiceException {

    public BadRequestException() {
        super("BadRequest");
    }

    public BadRequestException(String message) {
        super(message);
    }

}
