package br.com.frwkapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.frwkapp.abstracts.BaseRestController;
import br.com.frwkapp.abstracts.BaseService;
import br.com.frwkapp.model.domain.UserAuth;
import br.com.frwkapp.model.dto.UserDTO;
import br.com.frwkapp.model.service.UserService;

@RestController
@RequestMapping("user")
public class UserController extends BaseRestController<UserAuth, UserDTO> {

	@Autowired
    private UserService service;
    
	@Override
    protected BaseService<UserAuth, UserDTO> getService() {
        return service;
    }
	
	public List<UserDTO> findAll(String search) {
		return super.findAll(search);
	}
}