package br.com.frwkapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.frwkapp.abstracts.BaseResponseDTO;
import br.com.frwkapp.model.dto.LoginDTO;
import br.com.frwkapp.model.service.UserService;
import io.swagger.annotations.ApiImplicitParam;

@RestController
@RequestMapping("auth")
public class AuthController {

	@Autowired
    private UserService service;
	           
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public BaseResponseDTO login(@RequestBody LoginDTO dto) throws Exception {
    	return buildResponse(service.login(dto.getLogin(), dto.getPassword(), dto.getTokenFirebase()));
    }
    
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value="/refresh", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public BaseResponseDTO login(@RequestParam String refreshToken) {
    	return buildResponse(service.refreshLogin(refreshToken));
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value="/me", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    public BaseResponseDTO me() {
    	return buildResponse(service.parseToDTO(service.findByLogin(SecurityContextHolder.getContext().getAuthentication().getName())));
    }
    
    protected BaseResponseDTO buildResponse(Object object) {
		BaseResponseDTO response = new BaseResponseDTO();
		response.setData(object);
		return response;
	}
}