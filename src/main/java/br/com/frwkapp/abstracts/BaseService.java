package br.com.frwkapp.abstracts;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.frwkapp.model.domain.Tenant;

public abstract class BaseService<E extends BaseEntity, D extends BaseDTO> {

	public abstract BaseRepository<E> getRepository();

	public E findOne(Tenant tenant, Long id) {
		return getRepository().findFirstByTenantAndId(tenant, id);
	}

	public Page<E> findAll(Tenant tenant, String search, Pageable pageable) {
		if (search != null) {
			return getRepository().findAllByTenantAndIndexContainingIgnoreCase(tenant, search, pageable);
		} else {
			return getRepository().findAllByTenant(tenant, pageable);
		}
	}

	public List<E> findAll(Tenant empresa) {
		return getRepository().findAll();
	}

	public List<E> findAll(Tenant tenant, String search) {
		if (search != null) {
			return getRepository().findAllByTenantAndIndexContainingIgnoreCase(tenant, search);
		} else {
			return getRepository().findAllByTenant(tenant);
		}
	}

	public Page<E> findAll(Tenant empresa, Pageable pageable) {
		return getRepository().findAll(pageable);
	}

	public E insert(E entity) {
		if (validate(entity)) {
			entity.setIndex(entity.buildIndex());
			entity.setCreatedAt(new Date());
			getRepository().save(entity);
		}
		return entity;
	}

	public E update(E entity) {
		if (validate(entity)) {
			entity.setIndex(entity.buildIndex());
			entity.setLastUpdate(new Date());
			getRepository().save(entity);
		}
		return entity;
	}

	public E insert(D dto) {
		return insert(parseDtoToEntity(dto));
	}

	public E update(D dto) {
		return update(parseDtoToEntity(dto));
	}

	public void flush() {
		getRepository().flush();
	}

	public void delete(Tenant tenant, Long id) {
		getRepository().deleteByTenantAndId(tenant, id);
	}

	public boolean validate(E entity) {
		return true;
	}
	public abstract E parseDtoToEntity(D dto);
	public abstract D parseToDTO(E entity);
	public abstract List<D> parseToDTO(List<E> page);
	public abstract Page<D> parseToDTO(Page<E> page);

}