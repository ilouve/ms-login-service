package br.com.frwkapp.abstracts;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.commons.lang3.reflect.MethodUtils;

import br.com.frwkapp.model.domain.Tenant;
import lombok.Data;

@Data
@MappedSuperclass
public abstract class BaseEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6965115789737009173L;

    public abstract Long getId();
    public abstract void setId(Long id);


    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_update")
    private Date lastUpdate;
    
    @Column(name = "user_update")
    private String userUpdate;
    
    @Column(name = "deleted")
    private boolean deleted;

    @Column(name = "index")
    private String index;
    
    @ManyToOne
    @JoinColumn(name = "fk_tenant")
    private Tenant tenant;

    protected List<String> fieldsValuesToList() {
        List<String> words = new ArrayList<>();
        
        Field[] fields = FieldUtils.getFieldsWithAnnotation(this.getClass(), BuildIndex.class);
        
        for (Field field : fields) {
            try {
                if (field.getType().getSuperclass() == BaseEntity.class) {
                    
                        Object object = MethodUtils.invokeExactMethod(this, "get" + StringUtils.capitalize(field.getName()));
                        if (object != null) {
                            words.addAll(((BaseEntity) object).fieldsValuesToList());
                        }
                } else if (field.getType().getSuperclass() == Date.class) {
                        Object value = MethodUtils.invokeExactMethod(this, "get" + StringUtils.capitalize(field.getName()));
                        if (value != null) {
                            words.add(new SimpleDateFormat("dd/MM/yyyy").format((Date) value));
                        }
                } else {
                    Object value = MethodUtils.invokeExactMethod(this, "get" + StringUtils.capitalize(field.getName()));
                    if (value != null && value instanceof Date) {
                        words.add(new SimpleDateFormat("dd/MM/yyyy").format((Date) value));
                    } else if (value != null) {
                        words.add(String.valueOf(value));
                    }
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        return words;
    }

    public String buildIndex() {
       return StringUtils.join(fieldsValuesToList(), "");
    }
}