package br.com.frwkapp.abstracts;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.sql2o.Connection;
import org.sql2o.Sql2o;
import org.sql2o.quirks.PostgresQuirks;

import lombok.extern.java.Log;

@Log
public class BaseRepositoryImpl  {

    @PersistenceContext
    protected EntityManager entityManager;

    @Autowired
    private DataSource dataSource;


    protected <X> void setQueryPagination(TypedQuery<X> query, Pageable pageable) {
        query.setMaxResults(pageable.getPageSize());
        query.setFirstResult(new Long(pageable.getOffset()).intValue());
    }

    protected <X> void setQueryParameters(Map<String, Object> params, TypedQuery<X> query) {
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
    }

    protected <X> void setQueryPagination(Query query, Pageable pageable) {
        query.setMaxResults(pageable.getPageSize());
        query.setFirstResult(new Long(pageable.getOffset()).intValue());
    }

    protected <X> void setQueryParameters(Map<String, Object> params, Query query) {
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
    }

    protected void executeNativeScript(String sql) {
        executeNativeScript(sql, new HashMap<>());
    }

    protected void executeNativeScript(String sql, Map<String, Object> params) {
        Sql2o sql2o = new Sql2o(dataSource, new PostgresQuirks());

        try (Connection con = sql2o.open()) {
            org.sql2o.Query query = con.createQuery(sql);
            for (Map.Entry<String, Object> entry : params.entrySet()) {
                query.addParameter(entry.getKey(), entry.getValue());
            }

            query.executeUpdate();
            con.commit();
        }
    }

    protected <X> List<X> parseNativeQuery(String sqlQuery, Class<X> returnType) {
        return parseNativeQuery(sqlQuery, returnType, new HashMap<>());
    }

    protected <X> List<X> parseNativeQuery(String sqlQuery, Class<X> returnType, Map<String, Object> params) {
        Sql2o sql2o = new Sql2o(dataSource, new PostgresQuirks());

        try (Connection con = sql2o.open()) {
            org.sql2o.Query query = con.createQuery(sqlQuery);
            for (Map.Entry<String, Object> entry : params.entrySet()) {
                query.addParameter(entry.getKey(), entry.getValue());
            }
            return query.executeAndFetch(returnType);
        }
    }

    protected <X> X parseNativeQueryToSingleResult(String sqlQuery, Class<X> returnType) {
        return parseNativeQueryToSingleResult(sqlQuery, returnType, new HashMap<>());
    }

    protected <X> X parseNativeQueryToSingleResult(String sqlQuery, Class<X> returnType, Map<String, Object> params) {
        Sql2o sql2o = new Sql2o(dataSource, new PostgresQuirks());

        try (Connection con = sql2o.open()) {
            org.sql2o.Query query = con.createQuery(sqlQuery);
            for (Map.Entry<String, Object> entry : params.entrySet()) {
                query.addParameter(entry.getKey(), entry.getValue());
            }
            return query.executeAndFetchFirst(returnType);
        }
    }

    protected String reaplaceParams(String sqlQuery, Map<String, Object> params) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            if (entry.getValue() instanceof Date) {
                sqlQuery = sqlQuery.replaceAll(":" + entry.getKey(), "'" + dateFormat.format(entry.getValue()) + "'");
                continue;
            }

            if (entry.getValue() instanceof String) {
                sqlQuery = sqlQuery.replaceAll(":" + entry.getKey(), "'" + entry.getValue() + "'");
                continue;
            }

            if (entry.getValue() instanceof Number) {
                sqlQuery = sqlQuery.replaceAll(":" + entry.getKey(), entry.getValue() + "");
                continue;
            }

            sqlQuery = sqlQuery.replaceAll(":" + entry.getKey(), String.valueOf(entry.getValue()));
        }

        return sqlQuery;
    }
    
}
