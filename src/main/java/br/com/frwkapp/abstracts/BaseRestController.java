package br.com.frwkapp.abstracts;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.frwkapp.model.domain.Tenant;
import br.com.frwkapp.model.domain.UserAuth;
import br.com.frwkapp.model.service.UserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

public abstract class BaseRestController<E extends BaseEntity, D extends BaseDTO> {

	@Autowired
	private UserService userService;

	protected abstract BaseService<E, D> getService();

	protected UserAuth me() {
		return userService.findByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
	}

	protected Tenant getTenant() {
		try {
			UserAuth user = me();
			return user.getTenant();
		} catch (Throwable t) {
		}
		return null;
	}
	
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Find all")
	@RequestMapping(method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    public List<D> findAll(@RequestParam(value = "search", required = false) String search) {
		List<E> result = getService().findAll(getTenant(), search);
		return getService().parseToDTO(result);
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Find page")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "lastUpdate", value = "purpose of this property is to return only data with update more recent than this date - timeinmilis ", dataType = "long", paramType = "query"),
			@ApiImplicitParam(name = "page", value = "Current Page", dataType = "int", paramType = "query"),
			@ApiImplicitParam(name = "size", value = "Page Size", dataType = "int", paramType = "query") })
	@RequestMapping(value = "/page", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    public Page<D> findAll(@RequestParam(value = "search", required = false) String search, Pageable pageable) {
		Page<E> result = getService().findAll(getTenant(), search, pageable);
		return getService().parseToDTO(result);
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Find one by id")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "id of your entity", dataType = "long", paramType = "path") })
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    public D findOne(@PathVariable Long id) {
		E entity = getService().findOne(getTenant(), id);
		return getService().parseToDTO(entity);
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Create new")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    public D createNew(@RequestBody @Valid D dto) {

		E entity = getService().parseDtoToEntity(dto);
	    entity.setUserUpdate(me().getName());
		getService().insert(entity);

		return getService().parseToDTO(entity);
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Update an existing")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json;charset=UTF-8")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    public D update(@RequestBody @Valid D dto) {
		
		E entity = getService().parseDtoToEntity(dto);
		entity.setUserUpdate(me().getName());
		getService().update(entity);
		
		return getService().parseToDTO(entity);
	}

	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Delete one")
	@ApiImplicitParams({@ApiImplicitParam(name = "id", value = "id of your entity", dataType = "long", paramType = "path")})
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "application/json;charset=UTF-8")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    public void deleteOne(@PathVariable Long id) {
		getService().delete(getTenant(), id);
	}

	protected BaseResponseDTO buildResponse(Object object) {
		BaseResponseDTO response = new BaseResponseDTO();
		response.setData(object);
		return response;
	}

	protected BaseResponseDTO buildResponseMessage(String message) {
		BaseResponseDTO response = new BaseResponseDTO();
		response.setMessage(message);
		return response;
	}
}
