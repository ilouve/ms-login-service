package br.com.frwkapp.abstracts;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import br.com.frwkapp.model.domain.Tenant;

import java.util.List;

@NoRepositoryBean
public interface BaseRepository<E extends BaseEntity> extends JpaRepository<E, Long> {

	E findFirstByTenantAndId(Tenant tenant, Long id);
	List<E> findAllByTenant(Tenant tenant);
	Page<E> findAllByTenant(Tenant tenant, Pageable pageable);
    List<E> findAllByTenantAndIndexContainingIgnoreCase(Tenant tenant, String indice);
    Page<E> findAllByTenantAndIndexContainingIgnoreCase(Tenant tenant, String indice, Pageable pageable);
    void deleteByTenantAndId(Tenant tenant, Long id);
}
